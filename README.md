# superset-m4g

Ce projet contient les datasets, dashboards et charts utilisés dans Superset
pour visualiser les données du système NumEcoEval dans Superset.

## Pré-requis

Les datasets et les dashboards supposent que Superset a été connecté à la base de données 
des indicateurs de NumEcoEval.

## Fichiers

### Sprint 3
- [Répertoire sprint3_donneesSprint2](sprint3_donneesSprint2) Fichiers du Sprint 3
  - [Fichier datasets](sprint3_donneesSprint2/datasets_impactEquipement.yaml) : Exports des datasets utilisés par les dashboards
    - Pour le moment ne contient que la table des indicateurs ind_ImpactEquipement
  - [Fichier dashboards](sprint3_donneesSprint2/dashboard_Sprint3_20221011_104846.json) : Exports du dashboard du Sprint
    - Les dashboards ont besoin des datasets avant d'être importés dans Superset
    - Les dashboards contiennent également les charts 

## Licence
Licence Apache 2.0 utilisé.
Les fichiers peuvent être utilisés et modifiés à la convenance des utilisateurs.

## Statut du projet
Ces fichiers sont utilisables à partir du Sprint 2 du système NumEcoEval.

# Dashboards

## Sprint 3 avec données du Sprint 2

Le dashboard a été initialisé pour le Sprint 3 du MVP de NumEcoEval avec les données du Sprint 2.
Les données du Sprint 2 comprennent :
- L'impact des équipements
  - Incluant uniquement des équipements physiques
- L'impact sur le réseau : actuellement inutilisé dans les dashboards donc pas de datasets associés. 

<details>
  <summary>Exemple de dashboard pour le Sprint 3 avec les données du Sprint 2</summary>

<img src="sprint3_donneesSprint2/superset_Dashboard_avec_donnees_Sprint2_local.jpg">
</details>